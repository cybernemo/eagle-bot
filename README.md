# eagle-bot

Bot for Twitter using the v2 API

# Installation

```bash
# clone project
git clone https://gitlab.com/cybernemo/eagle-bot.git
cd eagle-bot
# bootstrap virtualenv
export VIRTUAL_ENV=.venv
mkdir -p $VIRTUAL_ENV
virtualenv $VIRTUAL_ENV
source $VIRTUAL_ENV/bin/activate
# install from PyPI
pip install -r requirements.txt
# set your bearer token
export BEARER_TOKEN=BEARER_TOKEN_FROM_TWITTER
```
