from pytwitter import Api
import os

# To set your enviornment variables in your terminal run the following line:
# export 'BEARER_TOKEN'='<your_bearer_token>'

# Retrieve bearer token
def auth():
    return os.environ.get("BEARER_TOKEN")


def main():
    bearer_token = auth()
    api = Api(bearer_token=bearer_token)
    print(api.get_users(usernames="elonmusk,ripple"))
    print(api.search_tweets(query="btc", query_type="all"))

if __name__ == "__main__":
    main()